package ru.tinkoff.ru.seminar;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;
import ru.tinkoff.ru.seminar.model.Weather;

public interface Api {

    @GET("/data/2.5/weather")
    Single<Weather> getWeather(@Query("q") String city, @Query("APPID") String appid, @Query("units") String units, @Query("lang") String lang);

    @GET("/data/2.5/forecast")
    Single<List<Weather>> getForecast(@Query("q") String city, @Query("APPID") String appid, @Query("units") String units, @Query("lang") String lang);

}
