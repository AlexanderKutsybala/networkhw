package ru.tinkoff.ru.seminar.model;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class ForecastDeserializer implements JsonDeserializer {
    @Override
    public Object deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        List<Weather> forcast = new ArrayList<>();
        JsonArray jsonArray = ((JsonObject) json).getAsJsonArray("list");
        for (JsonElement jsonElement : jsonArray) {

            JsonObject jsonObjectWeather = jsonElement.getAsJsonObject();
            JsonArray jsonArrayWeather = jsonObjectWeather.getAsJsonArray("weather");
            JsonElement jsonElemntWeather = jsonArrayWeather.get(0);
            JsonObject jsonObjectDescription = jsonElemntWeather.getAsJsonObject();
            JsonPrimitive jsonPrimitiveDescription = jsonObjectDescription.getAsJsonPrimitive("description");
            String description = jsonPrimitiveDescription.getAsString();

            JsonObject jsonObjectTemp = jsonObjectWeather.getAsJsonObject("main");
            JsonPrimitive jsonPrimitiveTemp = jsonObjectTemp.getAsJsonPrimitive("temp");
            float temp = jsonPrimitiveTemp.getAsFloat();

            JsonObject jsonObjectSpeed = jsonObjectWeather.getAsJsonObject("wind");
            JsonPrimitive jsonPrimitiveSpeedWind = jsonObjectSpeed.getAsJsonPrimitive("speed");
            float speedWind = jsonPrimitiveSpeedWind.getAsFloat();

            long time = jsonObjectWeather.get("dt").getAsLong() * 1000;
            forcast.add(new Weather(description, time, temp, speedWind));
        }
        return forcast;
    }
}