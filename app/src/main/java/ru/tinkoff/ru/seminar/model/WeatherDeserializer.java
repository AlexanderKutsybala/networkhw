package ru.tinkoff.ru.seminar.model;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

public class WeatherDeserializer implements JsonDeserializer<Weather> {
    @Override
    public Weather deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject jsonObject = json.getAsJsonObject();
        JsonObject mainJsonObject = jsonObject.getAsJsonObject("main");
        JsonObject windJsonObject = jsonObject.getAsJsonObject("wind");
        JsonArray weatherJsonArray = jsonObject.getAsJsonArray("weather");

        float temp = mainJsonObject.get("temp").getAsFloat();
        float windSpeed = windJsonObject.get("speed").getAsFloat();
        long time = jsonObject.get("dt").getAsLong() * 1000;

        JsonObject weatherObject = weatherJsonArray.get(0).getAsJsonObject();
        String description = weatherObject.get("description").getAsString();
        return new Weather(description, time, temp, windSpeed);
    }
}
